﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : MonoBehaviour {

    [SerializeField] List<Point> points;

    public void Init(float pointDisableTime)
    {        
        foreach (var point in points)
        {
            point.Init(pointDisableTime);
            point.OnEnablePoint.AddListener(BackToArray);
        }
    }

    public List<Point> GetPoints()
    {
        return points;
    }

    public bool IsAllPointsDisable()
    {
        return points.Count == 0;
    }

    public Point TakeRandomPoint()
    {
        int idx = Random.Range(0, points.Count);

        Point pickUpPoint = points[idx];
        points.RemoveAt(idx);

        pickUpPoint.Disable();

        return pickUpPoint;
    }

    #region private

    void BackToArray(Point point)
    {
        points.Add(point);
    }

    #endregion
}
