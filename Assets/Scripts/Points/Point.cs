﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnEnablePoint : UnityEvent<Point> { }

public class Point : MonoBehaviour {

    [HideInInspector] public EventOnEnablePoint OnEnablePoint = new EventOnEnablePoint();

    public void Init(float pointDisableTime)
    {
        this.pointDisableTime = pointDisableTime;

        position = transform.position;

        Vector3 direction = MIDDLE_OF_MAP - position;
        rotation = Quaternion.LookRotation(direction);
    }

    public void Disable()
    {
        Invoke(ENABLE, pointDisableTime);       
    }
    
    public void Enable()
    {
        OnEnablePoint.Invoke(this);
    }

    public Vector3 GetPosition()
    {
        return position;
    }

    public Quaternion GetRotation()
    {
        return rotation;
    }

    #region private

    const string ENABLE = "Enable";
    Vector3 MIDDLE_OF_MAP = new Vector3(0, 1, 0);

    float pointDisableTime;
    Vector3 position;
    Quaternion rotation;

    #endregion
}
