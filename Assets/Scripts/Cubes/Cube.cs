﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnDeadCube : UnityEvent<Cube> { }

public class Cube : MonoBehaviour {

    [HideInInspector] public EventOnDeadCube OnDeadCube = new EventOnDeadCube();

    public void Init(float speed)
    {
        this.speed = speed;
    }

    public void Enable()
    {
        gameObject.SetActive(true);
    }

    public void Disable()
    {
        OnDeadCube.Invoke(this);
        gameObject.SetActive(false);
    }

    #region private

    float speed;

    private void OnMouseUp()
    {
        Disable();
    }

    void Move()
    {
        Vector3 newPosition = transform.position + transform.forward * speed * Time.deltaTime;
        transform.SetPositionAndRotation(newPosition, transform.rotation);
    }

    private void Update()
    {
        Move();
    }

    #endregion
}
