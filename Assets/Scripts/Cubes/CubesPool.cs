﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesPool : MonoBehaviour {

    [SerializeField] Cube cube;
    [SerializeField] int countForFirstPutting;
    [SerializeField] int countForFollowingPutting;

    public void Init(float speed)
    {
        this.speed = speed;
        MakeNewCubes(countForFirstPutting);
    }

    public Cube TakeCube()
    {
        if (disableCubes.Count == 0)
        {
            MakeNewCubes(countForFollowingPutting);
        }

        Cube pickUpCube = disableCubes[0];
        disableCubes.RemoveAt(0);
        
        return pickUpCube;
    }

    public void PutCub(Cube cube)
    {
        disableCubes.Add(cube);
    }

    #region private

    List<Cube> disableCubes = new List<Cube>();
    float speed;

    void MakeNewCubes(int count)
    {
        for (int idx = 0; idx < count; idx++)
        {
            Cube newCube = Instantiate(cube, transform);
            newCube.Init(speed);
            newCube.gameObject.SetActive(false);
            disableCubes.Add(newCube);
            newCube.OnDeadCube.AddListener(PutCub);
        }
    }

    #endregion
}