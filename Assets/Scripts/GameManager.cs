﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    [Header("Prefabs")]
    [SerializeField] CubesPool cubesPool;
    [SerializeField] Points points;
    [SerializeField] Player player;
    [SerializeField] UI UI;

    [Header("Characteristics")]
    [SerializeField] int countOfCubesInOneRespawn;
    [SerializeField] float respawnCubesTime;
    [SerializeField] float speedOfCubes;
    [SerializeField] float pointDisableTime;

    #region private

    const string RESPAWN_NEW_CUBE = "RespawnNewCube";

    void Start ()
    {
        UI.Init();
        UI.OnClickFirstScreenButton.AddListener(HandleOnClickFirstScreenButton);
        UI.OnClickGameOverScreenButton.AddListener(HandleOnClickGameOverScreenButton);

        cubesPool.Init(speedOfCubes);
        points.Init(pointDisableTime);

        player.OnGameOver.AddListener(GameOver);
    }

    void HandleOnClickFirstScreenButton()
    {
        for (int idx = 0; idx < countOfCubesInOneRespawn; idx++)
        {
            Invoke(RESPAWN_NEW_CUBE, respawnCubesTime);
        }
    }

    void HandleOnClickGameOverScreenButton()
    {
        Cube[] allCubes = FindObjectsOfType<Cube>();
        foreach(var cube in allCubes)
        {
            if (cube.gameObject.activeSelf)
                cube.Disable();
        }

        player.ResetGameOver();
    }

    void RespawnNewCube()
    {
        if (points.IsAllPointsDisable() == true)
        {
            Invoke(RESPAWN_NEW_CUBE, respawnCubesTime);
            return;
        }

        Point newPoint = points.TakeRandomPoint();
        Cube newCube = cubesPool.TakeCube();

        newCube.transform.SetPositionAndRotation(newPoint.GetPosition(), newPoint.GetRotation());
        newCube.Enable();

        Invoke(RESPAWN_NEW_CUBE, respawnCubesTime);
    }

    void GameOver()
    {
        UI.EnableGameOverScreen();
    }

    #endregion
}
