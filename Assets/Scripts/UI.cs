﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI : MonoBehaviour {

    [HideInInspector] public UnityEvent OnClickFirstScreenButton = new UnityEvent();
    [HideInInspector] public UnityEvent OnClickGameOverScreenButton = new UnityEvent();

    [Header("FirstScreen")]
    [SerializeField] Image firstScreen;
    [SerializeField] Button firstScreenButton;

    [Header("GameOverScreen")]
    [SerializeField] Image gameOverScreen;
    [SerializeField] Button gameOverScreenButton;

    public void Init()
    {
        gameOverScreen.gameObject.SetActive(false);

        firstScreenButton.onClick.AddListener(HandleOnClickFirstScreenButton);
        gameOverScreenButton.onClick.AddListener(HandleOnClickGameOverScreenButton);
    }

    public void EnableGameOverScreen()
    {
        gameOverScreen.gameObject.SetActive(true);
    }

    #region private

    void HandleOnClickFirstScreenButton()
    {
        firstScreen.gameObject.SetActive(false);
        OnClickFirstScreenButton.Invoke();
    }

    void HandleOnClickGameOverScreenButton()
    {
        gameOverScreen.gameObject.SetActive(false);
        OnClickGameOverScreenButton.Invoke();
    }

    #endregion
}
