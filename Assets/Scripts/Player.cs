﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour {

    [HideInInspector] public UnityEvent OnGameOver = new UnityEvent();

    public bool GetIsGameOver()
    {
        return isGameOver;
    }

    public void ResetGameOver()
    {
        isGameOver = false;
    }

    #region private

    const string CUBE = "Cube";

    bool isGameOver = false;

    private void OnTriggerEnter(Collider other)
    {
        Cube badCube = other.gameObject.GetComponent<Cube>();

        if (!badCube)
            return;

        if (!isGameOver)
        {
            OnGameOver.Invoke();
            isGameOver = true;
        }

        badCube.Disable();
    }

    #endregion

}
